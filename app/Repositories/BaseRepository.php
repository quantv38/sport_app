<?php


namespace App\Repositories;



abstract class BaseRepository implements RepositoryInterface
{
    // Model tương tác
    protected $model;

    // Khởi tạo
    public function __construct() {
        $this->setModel();
    }

    // Lấy model tương ứng
    abstract public function getModel();

    public function setModel() {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    /**
     * @inheritDoc
     */
    public function getAll()
    {
        return $this->model->select()->get();
    }

    /**
     * @inheritDoc
     */
    public function find($id)
    {
        return $this->model->select()->where('id', $id)->get();
    }

    /**
     * @inheritDoc
     */
    public function create($attributes = [])
    {
        return $this->model->insert($attributes);
    }

    /**
     * @inheritDoc
     */
    public function update($id, $attributed = [])
    {
        return $this->model->where('id', $id)->update($attributed);
    }

    /**
     * @inheritDoc
     */
    public function delete($id)
    {
        return $this->model->where('id', $id)->delete();
    }
}
